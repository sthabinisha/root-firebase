package com.wlit.fellowship.atmnepal.utilities;

import android.support.v7.widget.RecyclerView;

/**
 * Created by root on 2/24/17.
 */

public interface OnStartDragListener {
    void onStartDrag(RecyclerView.ViewHolder viewHolder);
}
