package com.wlit.fellowship.atmnepal.helper;

/**
 * Created by root on 2/24/17.
 */

public interface ItemTouchHelperAdapter {
    boolean onItemMove(int fromPosition,int toPosition);
}
